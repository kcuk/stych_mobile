
import { Injectable } from "@angular/core";
import { Http,Response,RequestOptions,Headers,Request, RequestMethod } from "@angular/http";

import { Observable } from "rxjs";
//import { map, catch } from 'rxjs/operators';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';




@Injectable({
  providedIn: 'root'
})
export class PostService {
  progress:boolean=false;
  //======== base path ========//
  
  public base_path: string;
  public base_path_img: string;
  public base: string;
  //======== API ========//
  public headers: Headers;
  public requestoptions: RequestOptions;
  public res: Response;
  public serverError: boolean;

  constructor(public http: Http) {
    
    this.base_path = 'https://stych.sia.co.in/app/v1/contact_us/';
  }

public postRequest(url: string, data: any): any {
 
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
   

    this.requestoptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: this.headers,
      body: JSON.stringify(data)
    });

    return this.http
      .request(new Request(this.requestoptions))
      .map((res: Response) => {
      
        if (res) {
          return [{ status: res.status, json: res.json() }];
        }
      })
      .catch(error => {
        return Observable.throw(error);
      });
  }
}